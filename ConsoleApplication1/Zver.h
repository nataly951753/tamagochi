#pragma once
#include "function.h"
using namespace std;

class Zver
{
	bool death;//���� ��� ���?
	vector <Food> food;//������ ���
	vector <string> icon;//������ ���������
	int live;//��������
	int sutost;//�������
	int happy;//�������
	int kx;//���������� �����
	int ky;//���������� �����
	Color color;//����
	string name;//���
	Data Birthday;//���� ��������
	Data Death;//���� ������
	int type;//��� �������
	bool staydied;//���� ������ �����������

public:
	Zver(string na, int t, Data k)//�����������
	{
		staydied = false;//���� ������ �� �����������
		live = 100;//��������� ����������
		sutost = 100;
		happy = 100;
		setColor(t);//����
		setName(na);//���
		kx = 20;
		ky = 10;
		Birthday.day = k.day;//���� ��������
		Birthday.hours = k.hours;
		Birthday.min = k.min;
		Birthday.year = k.year;
		Birthday.month = k.month;
		Birthday.sec = k.sec;
		death = false;//�������� ���
	}
	bool getstaydied()//����������� �� ���� ������?
	{
		return staydied;
	}
	int gettype()
	{
		return type;
	}
	Data getBirthday()
	{
		return Birthday;
	}
	void setdeath(bool k)//������������� ���� ���� ��� ���
	{
		death = k;
	}
	void setstaydied(bool k)
	{
		staydied = k;
	}
	bool getdeath()//����� ��� ���?
	{
		return death;
	}
	Data Age(Data start, Data end)//���������� ������� �����
	{
		Data Age;
		int difyear = end.year - start.year;//������� � �����
		if (difyear > 0)
		{
			if (end.month >= start.month)
			{
				Age.year = difyear;
			}
			else
			{
				Age.year = difyear - 1;//���� ��� ���������, �� 12 ������� �� ������
			}
		}
		else
		{
			Age.year = difyear;
		}
		int difmonth;//������� � �������
		if (end.month >= start.month)
		{
			difmonth = end.month - start.month;
		}
		else
		{
			difmonth = 11 - start.month + end.month;
		}
		if (difmonth > 0)
		{
			if (end.day >= start.day)
			{
				Age.month = difmonth;
			}
			else
			{
				Age.month = difmonth - 1;//���� ����� ��������� �� 31 ���� �� ������

			}
		}
		else
		{
			Age.month = difmonth;
		}

		int difday;//������� � ����
		if (end.day >= start.day)
		{
			difday = end.day - start.day;
		}
		else
		{
			difday = 31 - start.day + end.day;
		}
		if (difday > 0)
		{
			if (end.hours >= start.hours)
			{
				Age.day = difday;
			}
			else
			{
				Age.day = difday - 1;//����� �� ������
			}
		}
		else
		{
			Age.day = difday;
		}
		int difhour;//������� � �����
		if (end.hours >= start.hours)
		{
			difhour = end.hours - start.hours;
		}
		else
		{
			difhour = 23 - start.hours + end.hours;
		}
		if (difhour > 0)
		{
			if (end.min >= start.min)
			{
				Age.hours = difhour;
			}
			else
			{
				Age.hours = difhour - 1;//60����� �� ������
			}
		}
		else
		{
			Age.hours = difhour;
		}
		int difmin;//������� � �������
		if (end.min >= start.min)
		{
			difmin = end.min - start.min;
		}
		else
		{
			difmin = 59 - start.min + end.min;
		}
		if (difmin > 0)
		{
			if (end.sec >= start.sec)
			{
				Age.min = difmin;
			}
			else
			{
				Age.min = difmin - 1;
			}
		}
		else
		{
			Age.min = difmin;
		}
		return Age;
	}
	void life()//��� �� ����?
	{
		if (happy == 0 || live == 0 || sutost == 0)
		{
			setdeath(true);//���� ����
		}
	}
	void setdatadeath(int mind, int hourd, int dayd, int mond, int yeard)//������������� ���� ������
	{
		setstaydied(true);
		if (dayd > 31)
		{
			dayd -= 31;
			mond++;
		}
		if (mond > 11)
		{
			mond -= 11;
			yeard++;
		}
		Death.day = dayd;
		Death.hours = hourd;
		Death.min = mind;
		Death.month = mond;
		Death.year = yeard;
	}
	Data getdatadeath()//���������� ���� ������
	{
		return Death;
	}
	int kxord()//���������� ���������� ����� �� ������� ������
	{
		return kx;
	}
	int kyord()//���������� ���������� ����� �� ������� ������
	{
		return ky;
	}
	void setkx(int x)//������������� ���������� ����� �� ������� ������
	{
		kx = x;
	}
	void setky(int y)//������������� ���������� ����� �� ������� ������
	{
		ky = y;
	}
	string getName()//���������� ��� �����
	{
		return name;
	}
	Color getColor()//���������� ���� �����
	{
		return color;
	}
	void setfood(string path)//��������� ������ ����� �� �����
	{
		Food foood;//������� ������ ���� ��������� ��������
		ifstream fin;//����
		fin.open(path.c_str(), ios_base::binary | ios_base::in);//��������� ���� ��� ������
		if (fin.is_open() != 0)//���� ���� ������� �������
		{
			while (!fin.eof()) //���� �� ������ �� ����� �����
			{
				fin >> foood.name >> foood.kaloriynost;//��������� ���������
				food.push_back(foood);//��������� ������� � ������� ���
			}

		}
		fin.close();//��������� �����
	}
	int getlive()//���������� ��������� �������� �����
	{
		return live;
	}
	int getsutost()//���������� ��������� ��������� �����
	{
		return sutost;
	}
	int gethappy()//���������� ��������� ������� �����
	{
		return happy;
	}
	void setColor(int t)//������������� ���� �����
	{
		color = (Color)t;
	}
	void setHappy(int t)//������������� ��������� ������� �����
	{
		happy = t;
		if (happy > 100)
		{
			happy = 100;
		}
		if (happy < 0)
		{
			happy = 0;
		}
	}
	void setlive(int t)//������������� ��������� �������� �����
	{
		live = t;
		if (live > 100)
		{
			live = 100;
		}
		if (live < 0)
		{
			live = 0;
		}

	}
	void setsutost(int t)//������������� ��������� ��������� �����
	{
		sutost = t;
		if (sutost > 100)
		{
			sutost = 100;
		}
		if (sutost < 0)
		{
			sutost = 0;
		}
	}
	void settype(int k)
	{
		type = k;
	}
	void setName(string n)//������������� ��� �����
	{
		name = n;
	}
	void setIcon(string path)//������������� �������� �����
	{
		ifstream fin;
		fin.open(path);
		char buff[45];
		if (fin.is_open() != 0)
		{
			while (!fin.eof()) {
				fin.getline(buff, 45);
				icon.push_back(buff);
			}
			fin.close();
		}
	}
	void showObj(int kx, int ky, int Colour)//������������ �����
	{
		SetColor((Color)Colour, Black);
		for (int i = 0; i < icon.size(); i++)
		{
			setCursor(kx, ky);
			cout << icon[i];
			ky++;
		}
	}
	void MovePets()//������������ ����� �� �������� ������
	{
		int naprav = 1 + rand() % 4;//����� ����������� ��������

		if (naprav == 1)//�����
		{
			if (kyord() != 7)
			{
				showObj(kxord(), kyord(), Black);
				setky(kyord() - 1);//������ ����������
				showObj(kxord(), kyord(), getColor());
			}
		}
		else if (naprav == 2)//����
		{
			if (kyord() != 19)
			{
				showObj(kxord(), kyord(), Black);
				setky(kyord() + 1);//������ ����������
				showObj(kxord(), kyord(), getColor());
			}
		}
		else if (naprav == 3)//�����
		{
			if (kxord() != 0)
			{
				showObj(kxord(), kyord(), Black);
				setkx(kxord() - 1);//������ ����������
				showObj(kxord(), kyord(), getColor());
			}
		}
		else if (naprav == 4)//������
		{
			if (kxord() != 70)
			{
				showObj(kxord(), kyord(), Black);
				setkx(kxord() + 1);//������ ����������
				showObj(kxord(), kyord(), getColor());
			}
		}


	}
	void Pokazateli(int hap, int health, int hunger)//����������� � ��������� ��������� ����������� �����
	{

		SetColor(LightGray, Black);
		setCursor(8, 3);
		cout << "�������";
		setCursor(50, 3);
		cout << "��������";
		setCursor(90, 3);
		cout << "�������";

		//��������� �����������
		setHappy(gethappy() + hap);
		setlive(getlive() + health);
		setsutost(getsutost() + hunger);

		//���������
		for (size_t i = 0, z = 2; i < 20; i++)
		{
			setCursor(z, 4);
			z++;
			SetColor(Black, Black);
			cout << " ";
		}
		for (size_t i = 0, z = 2; i < 20; i++)
		{
			setCursor(z, 5);
			z++;
			SetColor(Black, Black);
			cout << " ";
		}
		//����� ��������� � ���������
		if (getsutost() < 50)
		{

			setCursor(2, 5);

			SetColor(LightRed, Black);

			cout << "��������� �������!";
		}
		//��������� ����������
		for (size_t i = 0, z = 2; i < (getsutost() / 5); i++)
		{
			setCursor(z, 4);
			z++;
			SetColor(LightGreen, LightGreen);
			if (getsutost() < 60)//���� ��������� ������ 60 ���������� ������
			{
				SetColor(Yellow, Yellow);
			}
			if (getsutost() < 30)//���� ��������� ������ 30 ���������� �������
			{
				SetColor(LightRed, LightRed);
			}
			cout << " ";
		}
		//��������� ��������� � ���������
		setCursor(23, 4);
		SetColor(LightGray, Black);
		cout << getsutost() << "%  ";
		for (size_t i = 0, z = 45; i < 20; i++)
		{
			setCursor(z, 4);
			z++;
			SetColor(Black, Black);
			cout << " ";
		}
		for (size_t i = 0, z = 45; i < 20; i++)
		{
			setCursor(z, 5);
			z++;
			SetColor(Black, Black);
			cout << " ";
		}
		if (getlive() < 50)
		{

			setCursor(45, 5);

			SetColor(LightRed, Black);

			cout << "��������� �������!";
		}
		for (size_t i = 0, z = 45; i < (getlive() / 5); i++)
		{
			setCursor(z, 4);
			z++;

			SetColor(LightGreen, LightGreen);

			if (getlive() < 60)
			{
				SetColor(Yellow, Yellow);
			}
			if (getlive() < 30)
			{
				SetColor(LightRed, LightRed);
			}
			cout << " ";
		}

		setCursor(66, 4);
		SetColor(LightGray, Black);

		cout << getlive() << "%  ";
		for (size_t i = 0, z = 85; i < 20; i++)
		{
			setCursor(z, 4);
			z++;
			SetColor(Black, Black);
			cout << " ";
		}
		for (size_t i = 0, z = 85; i < 20; i++)
		{
			setCursor(z, 5);
			z++;
			SetColor(Black, Black);
			cout << " ";
		}
		if (happy < 50)
		{

			setCursor(85, 5);

			SetColor(LightRed, Black);

			cout << "����������� �������!";
		}
		for (size_t i = 0, z = 85; i < (gethappy() / 5); i++)
		{
			setCursor(z, 4);
			z++;
			SetColor(LightGreen, LightGreen);
			if (gethappy() < 60)
			{
				SetColor(Yellow, Yellow);
			}
			if (gethappy() < 30)
			{
				SetColor(LightRed, LightRed);
			}
			cout << " ";
		}
		setCursor(106, 4);
		SetColor(LightGray, Black);

		cout << gethappy() << "%  ";

	}
	void Move()//������������ ����� � ����������� ������
	{
		system("cls");
		setCursor(30, 20);
		SetColor(LightCyan, Black);
		cout << "��� ������������ ����������� �������, ��� ������ Esc";
		setCursor(35, 23);

		system("pause");
		system("cls");
		int i_input = 77, ky = 10, kx = 10;
		time_t dostart, timepoint;//�������� ����� ��� �������� ��������� ����������� � �������� ��������� ������
		struct tm  doinfo, infopoint;//����������� ��������� ��� �������� ����
		errno_t errdo, errpoint;
		time(&dostart);//�������� ������� �����
		errdo = localtime_s(&doinfo, &dostart);//��������� ���������� �������� �� ���� ���������
		time(&timepoint);//�������� ������� �����
		errdo = localtime_s(&infopoint, &timepoint);//��������� ���������� �������� �� ���� ���������
		Pokazateli(0, 0, 0);
		SetColor(Yellow, Black);
		setCursor(103, 2);
		cout << "������: " << user.money;

		int kxp, kyp;//���������� ������
		bool food, money, health;//���� ������
		bool point = false;//���� �� ���� ������ ��� ���
		char znak;//������ ������
		int sum;//������ ������

		do
		{
			life();//��������� ��� �� ����
			time(&nowtime);//�������� ������� �����
			errnow = localtime_s(&nowinfo, &nowtime);//��������� ���������� �������� �� ���� ���������

			if ((point == false) && (difsec(nowinfo.tm_sec, infopoint.tm_sec) > 10))//�� ���� ��� ������ � ������ 10 ��� ����� �������� ������
			{

				point = true;//������ ���������
				food = false;
				money = false;
				health = false;
				int r = 1 + rand() % 3;//�������� ��� ������
				if (r == 1)
				{
					food = true;
					znak = '*';
					SetColor(LightGreen, Black);
					sum = 5;

				}
				else if (r == 2)
				{
					money = true;
					znak = '$';
					SetColor(Yellow, Black);
					sum = 50;

				}
				else
				{
					health = true;
					znak = '+';
					SetColor(LightRed, Black);
					sum = 5;
				}
				do {
					kxp = rand() % 100;
					kyp = 7 + rand() % 30;
				} while ((kyp >= ky && kyp <= (ky + 19)) && (kxp >= kx && kxp <= (kx + 35)));
				setCursor(kxp, kyp);
				cout << znak;
			}
			//������ 20 ��� �������� ����������
			if ((difsec(nowinfo.tm_sec, doinfo.tm_sec) > 20))
			{
				time(&dostart);//�������� ������� �����
				errdo = localtime_s(&doinfo, &dostart);//��������� ���������� �������� �� ���� ���������

				Pokazateli(10, -1, -2);
			}


			switch (_kbhit())//�������� ������ �� �������
			{
			case 1: // ���� ������� ������ 
			{
				i_input = _getch();//����������� ������� �������
				switch (i_input)
				{
				case 72://�����
				{
					if (ky < 7)
					{
						break;
					}
					else {
						showObj(kx, ky, Black);//������������
						ky--;//�������� ����������
						showObj(kx, ky, getColor());//������������
					}


				} break;
				case 80://����
				{
					if (ky > 20)
					{
						break;
					}
					else {
						showObj(kx, ky, Black);
						ky++;
						showObj(kx, ky, getColor());
					}

				} break;
				case 75://�����
				{
					if (kx < 1)
					{
						break;
					}
					else {
						showObj(kx, ky, Black);
						kx--;
						showObj(kx, ky, getColor());
					}

				}break;
				case 77://������
				{
					if (kx > 78)
					{
						break;
					}
					else {
						showObj(kx, ky, Black);
						kx++;
						showObj(kx, ky, getColor());
					}

				}break;
				}
			}break;

			}
			if ((point == true) && (kyp >= ky && kyp <= (ky + 19)) && (kxp >= kx && kxp <= (kx + 35)))//������� ���� ������
			{
				//��������� ������� ������
				time(&timepoint);//�������� ������� �����
				errdo = localtime_s(&infopoint, &timepoint);//��������� ���������� �������� �� ���� ���������
				point = false;//������ ���
				if (food == true)
				{
					setsutost(getsutost() + sum);
				}
				else if (money == true)
				{
					user.money += sum;
				}
				else
				{
					setlive(getlive() + sum);
				}
				SetColor(Yellow, Black);
				setCursor(103, 2);
				cout << "������: " << user.money;
				Pokazateli(0, 0, 0);
			}
		} while ((i_input != 27) && (getdeath() != true));//���� �� ����� Enter

		SetColor(White, Black);
		system("cls");
	}
	void godmore()//����������
	{
		happy = 50;
		live = 50;
		sutost = 50;
		death = false;
	}
	vector<Food> getmas()//������ ��� �����
	{
		return food;
	}
	int menufood()//����� �������� �� ������ ��� �����
	{
		setCursor(27, 0);
		SetColor(LightBlue, Black);
		cout << "����������� �������, ������, ������� Enter, ��� ������ - Esc";
		setCursor(105, 2);
		SetColor(Yellow, Black);
		cout << "������: " << user.money;
		int i_input;//����������, �������� ���� ������� ������
		int kxvubor = 1;//��������� �������

		do {
			for (size_t i = 0, z = 15; i < 3; i++)
			{
				setCursor(z, 20);
				if (kxvubor == i)//������������ ��������� �������
				{
					SetColor(LightCyan, Black);
				}
				else
				{
					SetColor(LightGray, Black);
				}
				cout << getmas()[i].name.c_str() << " ������������ " << getmas()[i].kaloriynost;
				setCursor(z, 22);
				cout << "���� " << (getmas()[i].kaloriynost * 2);
				z += 30;
			}


			switch (i_input = _getch())//�������� ������ �� �������
			{
			case 75://�����
			{
				if (kxvubor != 0)
				{
					kxvubor--;
				}
				else
				{
					break;
				}
			} break;
			case 77://������
			{
				if (kxvubor != 2)
				{
					kxvubor++;
				}
				else
				{
					break;
				}
			} break;
			case 13://Enter
			{
				if (user.money < (getmas()[kxvubor].kaloriynost * 2))
				{
					system("cls");
					SetColor(LightRed, Black);
					setCursor(45, 20);
					cout << "� ��� ������������ �����!!!";
					setCursor(38, 22);
					system("pause");
					system("cls");
					kxvubor = menufood();
				}

			} break;
			case 27://Esc
			{
				kxvubor = -1;

			} break;
			}
		} while (i_input != 13 && i_input != 27);//���� �� ����������� �����
		return kxvubor;
	}
	void Play()//���� � ��������
	{
		system("cls");
		setCursor(86, 21);
		SetColor(LightGray, Black);
		cout << "�������� �� ������� �������";
		setCursor(82, 23);
		cout << "����������� �������, ��� ������ - Esc";
		setCursor(105, 2);
		SetColor(Yellow, Black);
		cout << "������: " << user.money;
		vector <string> U;//������ ��� �������� ����� �� ������ (�������� ����)
		ifstream fin;
		fin.open("play.txt");
		char buff[80];
		if (fin.is_open() != 0)
		{

			while (!fin.eof()) //�� ����� �����
			{
				fin.getline(buff, 80);
				U.push_back(buff);
			}
			fin.close();
		}

		int klassikrand = 1/*�������, �� ������� ����� �������*/, klassikvubor = 1/*�������, �� ������� ��������� �����*/, i_input = 0;
		do {
			life();//��������� ��� �� ����
			if (klassikrand == klassikvubor)/*���� ����� ������ ������� ��������*/
			{
				setCursor(105, 2);
				SetColor(Black, Black);
				cout << "������: " << user.money;
				user.money += (klassikrand * 10);
				setCursor(105, 2);
				SetColor(Yellow, Black);
				cout << "������: " << user.money;
				//�������� ��������� ����������
				setHappy(gethappy() + 5);
				setlive(getlive() - 1);
				setsutost(getsutost() - 2);
				//�������� �������� ������� ��������
				do {
					klassikrand = 1 + rand() % 10;
				} while (klassikrand == klassikvubor);
				//��������� ��������� �����������
				SetColor(LightGray, Black);
				setCursor(8, 3);
				cout << "�������";
				setCursor(10, 20);
				cout << "��������";
				setCursor(10, 37);
				cout << "�������";

				for (size_t i = 0, z = 2; i < 20; i++)
				{
					setCursor(z, 4);
					z++;
					SetColor(Black, Black);
					cout << " ";
				}
				for (size_t i = 0, z = 2; i < 20; i++)
				{
					setCursor(z, 5);
					z++;
					SetColor(Black, Black);
					cout << " ";
				}
				if (getsutost() < 50)
				{

					setCursor(2, 5);

					SetColor(LightRed, Black);

					cout << "��������� �������!";
				}
				for (size_t i = 0, z = 2; i < (getsutost() / 5); i++)
				{
					setCursor(z, 4);
					z++;
					SetColor(LightGreen, LightGreen);
					if (getsutost() < 60)
					{
						SetColor(Yellow, Yellow);
					}
					if (getsutost() < 30)
					{
						SetColor(LightRed, LightRed);
					}
					cout << " ";
				}
				setCursor(23, 4);
				SetColor(LightGray, Black);

				cout << getsutost() << "%  ";
				for (size_t i = 0, z = 2; i < 20; i++)
				{
					setCursor(z, 21);
					z++;
					SetColor(Black, Black);
					cout << " ";
				}
				for (size_t i = 0, z = 2; i < 20; i++)
				{
					setCursor(z, 22);
					z++;
					SetColor(Black, Black);
					cout << " ";
				}
				if (getlive() < 50)
				{

					setCursor(2, 22);

					SetColor(LightRed, Black);

					cout << "��������� �������!";
				}
				for (size_t i = 0, z = 2; i < (getlive() / 5); i++)
				{
					setCursor(z, 21);
					z++;

					SetColor(LightGreen, LightGreen);

					if (getlive() < 60)
					{
						SetColor(Yellow, Yellow);
					}
					if (getlive() < 30)
					{
						SetColor(LightRed, LightRed);
					}
					cout << " ";
				}

				setCursor(23, 21);
				SetColor(LightGray, Black);

				cout << getlive() << "%  ";
				for (size_t i = 0, z = 2; i < 20; i++)
				{
					setCursor(z, 38);
					z++;
					SetColor(Black, Black);
					cout << " ";
				}
				for (size_t i = 0, z = 2; i < 20; i++)
				{
					setCursor(z, 39);
					z++;
					SetColor(Black, Black);
					cout << " ";
				}
				if (happy < 50)
				{

					setCursor(2, 39);

					SetColor(LightRed, Black);

					cout << "����������� �������!";
				}
				for (size_t i = 0, z = 2; i < (gethappy() / 5); i++)
				{
					setCursor(z, 38);
					z++;
					SetColor(LightGreen, LightGreen);
					if (gethappy() < 60)
					{
						SetColor(Yellow, Yellow);
					}
					if (gethappy() < 30)
					{
						SetColor(LightRed, LightRed);
					}
					cout << " ";
				}
				setCursor(23, 38);
				SetColor(LightGray, Black);

				cout << gethappy() << "%  ";
			}
			int x = 40;//���������� ����������� ������ ���������
			bool flag = true;//�������� �� �������� �������� ������ �� ���������
			int klassik = 10;//����� ��������
			for (int i = 0, z = 0, t = 1; i < U.size(); i++)
			{
				SetColor(LightGray, Black);
				if (t % 5 == 0 && (klassik == 7 || klassik == 4))
				{
					klassik--;
					t = 0;
				}
				else if (t % 6 == 0 && klassik == 10)
				{
					klassik--;
					t = 0;
				}
				else if (t % 7 == 0 && klassik != 10)
				{
					klassik--;
					t = 0;
				}
				//��������� ������� ��������, � ��������, �� ������� ��������� �����
				if (klassik == klassikrand)
				{
					SetColor(LightMagenta, Black);
				}
				if (klassik == klassikvubor)
				{
					SetColor(LightCyan, Black);
				}
				setCursor(x, z);
				cout << U[i];
				z++; t++;
				//��������� ��������� ����� ������� ���������
				if (z == 12)
				{
					if (flag == true)//������ �������
					{
						z = 5;
						x = 60;
						flag = false;
					}
					else//����� �������
					{
						x = 40;
						flag = true;
					}
				}
				if (z == 24)
				{
					if (flag == true)
					{
						z = 17;
						x = 60;
						flag = false;
					}
					else
					{
						x = 40;
						flag = true;
					}
				}
				if (z == 36)
				{
					if (flag == true)
					{
						z = 29;
						x = 60;
						flag = false;
					}
					else
					{
						x = 40;
						flag = true;
					}
				}
			}

			i_input = _getch();//����������� ���������� ��� ������� �������

			switch (i_input)//�������� ������ �� �������
			{
			case 75://�����
			{
				if (klassikvubor == 8 || klassikvubor == 5 || klassikvubor == 2)
				{
					klassikvubor++;
				}
			} break;
			case 77://������
			{
				if (klassikvubor == 9 || klassikvubor == 6 || klassikvubor == 3)
				{
					klassikvubor--;
				}

			} break;
			case 72://�����
			{
				if (klassikvubor == 1 || klassikvubor == 3 || klassikvubor == 4 || klassikvubor == 6 || klassikvubor == 7 || klassikvubor == 9)
				{
					klassikvubor++;
				}

			} break;
			case 80://����
			{
				if (klassikvubor == 2 || klassikvubor == 4 || klassikvubor == 5 || klassikvubor == 7 || klassikvubor == 8 || klassikvubor == 10)
				{
					klassikvubor--;
				}

			} break;
			}
		} while ((i_input != 27) && (getdeath() != true));
		system("cls");
	}
	void DiedOfTime(Data save, int koef)//��� �������� ����������� �������� ��������� ��� �� ��, ��� ������ ������� ����� ���� 
	{
		time(&nowtime);//�������� ������� �����
		errnow = localtime_s(&nowinfo, &nowtime);//��������� ���������� �������� �� ���� ���������
												 //������� ������� � �����
		Data end;
		Data start = save;//�� ���� ����������
						  //�� ������
		end.day = nowinfo.tm_mday;
		end.hours = nowinfo.tm_hour;
		end.min = nowinfo.tm_min;
		end.month = nowinfo.tm_mon;
		end.sec = nowinfo.tm_sec;
		end.year = nowinfo.tm_year;
		Data age = Age(start, end);

		//���� ���� ������ ������ ��� ����� ������� ������� ��� ������������� ������������ - �� �������
		if (age.year > 0 || age.month > 0 || (age.day >= koef))
		{
			//���������� ���������� �� 0
			setHappy(0);
			setsutost(0);
			setlive(0);
			//������������� ���� ������ � ������ ����, ������� ��� ������� �������
			setdatadeath(save.min, save.hours, (save.day + koef), save.month, save.year);

		}
		//���� ������� �� ����, �������� �� ����������� ��, ������� ����� �� ���� ������������ � ����
		else
		{
			//��-�� ����, ��� ����� ����� ������� ��� ������������� ������ ���-�� ����, �������� ������ ���-�� ������ �� �����������, � ����������� �� �����������
			int hour = ((age.day * 24 + age.hours) / koef);
			for (size_t i = 0; i < hour; i++)
			{
				setHappy(gethappy() - i);
				setsutost(getsutost() - i);
				setlive(getlive() - i);
				//���� ���� �� ����������� ���������� ����� 0, ���� �������
				if (getlive() == 0 || getsutost() == 0 || gethappy() == 0)
				{
					setdatadeath(save.min, (save.hours + i), save.day, save.month, save.year);
					break;
				}
			}
		}

	}
	void Save(string path)//���������� ������ � �������
	{
		ofstream fout(path, ios_base::binary | ios_base::trunc);//������ � �������������� ��������� �����������
		time(&nowtime);//�������� ������� �����
		errnow = localtime_s(&nowinfo, &nowtime);//��������� ���������� �������� �� ���� ���������
		Data save = getBirthday();//�������� ���� ��������
		if (fout.is_open() != 0)
		{
			fout << "true "/*������� �������*/ << gettype() /*��� �������*/ << " " << getName() << " "
				<< getColor() << " " << save.sec << " "
				<< save.min << " " << save.hours << " "
				<< save.day << " " << save.month << " "
				<< save.year << " " << getdeath() << " "
				<< gethappy() << " " << getlive() << " "
				<< getsutost() << " " << /*����� �� ������ ����������*/nowinfo.tm_sec << " "
				<< nowinfo.tm_min << " " << nowinfo.tm_hour << " "
				<< nowinfo.tm_mday << " " << nowinfo.tm_mon << " "
				<< nowinfo.tm_year;
		}
		else
		{
			cout << "������ ������";
		}
		fout.close();
	}
	void SaveTable(string path)//���������� ���������� � ��������� ����
	{
		ofstream fout;//���������� �����

		if (getdeath() == true)//���� ���� ����� ��������� ��� � ������ ������� ������
		{
			fout.open(path, ios_base::binary | ios_base::app);

		}
		else//���� ��� �������������� ���� � �����
		{
			fout.open(path, ios_base::binary | ios_base::trunc);

		}
		//������� ���
		if (gettype() == 1)
		{
			fout << " ����� ";
		}
		else if (gettype() == 2)
		{
			fout << " ������� ";
		}
		else if (gettype() == 3)
		{
			fout << " ����� ";
		}
		//������� ��� �������� � ������������
		fout << getName() << " " << user.login << " ";
		//������� �������
		Data start = getBirthday();//�� ��� ��������
		Data end;
		if (getdeath() == true)//���� ����� �� ��� ������
		{
			end = getdatadeath();
		}
		else//���� ��� �� ������
		{
			end.day = nowinfo.tm_mday;
			end.hours = nowinfo.tm_hour;
			end.min = nowinfo.tm_min;
			end.month = nowinfo.tm_mon;
			end.sec = nowinfo.tm_sec;
			end.year = nowinfo.tm_year;
		}
		Data age = Age(start, end);
		fout << age.year << " " << age.month << " " << age.day << " " << age.hours << " " << age.min;
		if (getdeath() == true)
		{
			fout << " �����";
		}
		else
		{
			fout << " ���";
		}
		fout.close();
	}


	virtual void Eat() = 0;//���������
	virtual void Help() = 0;//���������
	virtual void Sound() = 0;//��������


							 //����� ����������� �������
							 //�����, � ������� ���� ���� �� ���� ����� ����������� ������� - �����������.
							 //������ ��������� ���������� ������ ������. ��������� ��������� ����� :)
};

Zver *pets[3];//������ ��������
